// import 'promise-polyfill';
// import 'isomorphic-fetch';
import 'babel-polyfill';
import 'url-search-params-polyfill';

import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createBrowserHistory} from 'history';
import config from '../config';

import './style';
import configureStore from "./store/configureStore";

import styleTemplate from '../style_template_1';
import testData from '../test_map';
import ObjectModel from './containers/map/ObjectModel';

let root;


function setupHotModuleReloading() {
	// in development, set up HMR:
	if (module.hot) {
		// require('preact/devtools');   // turn this on if you want to enable React DevTools!
		module.hot.accept('./containers/app', () => requestAnimationFrame(init));
	}
}

function initPwa() {
	// register ServiceWorker via OfflinePlugin, for prod only:
	if (process.env.NODE_ENV === 'production') {
		require('./pwa');
	}
}


async function init() {
	const model = ObjectModel.unserialize(testData, styleTemplate);
	global.model = model;
	const history = createBrowserHistory();
	let App = require('./containers/app').default;
	const store = configureStore(history);

	ReactDOM.render(
		<App
			store={store}
			history={history}
		/>,
		document.getElementById('app'));

	initPwa();
	setupHotModuleReloading();
}

init();
