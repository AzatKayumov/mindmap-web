import React, {Component} from 'react';
import {Switch, Route, Redirect, withRouter} from 'react-router-dom';

const RedirectOnLoginRoute = ({component: Component, isLoggedIn, isLoginProcessing, redirectTo = '/work', withParams, ...rest}) => (
	<Route
		{...rest}
		render={props => {
			if (withParams) {
				redirectTo += rest.location.search;
			}

			if (isLoggedIn && !isLoginProcessing) {
				return (
					<Redirect
						to={{
							pathname: redirectTo,
							state: {from: props.location}
						}}
					/>
				);
			}

			return (
				<Component {...props} />
			);
		}}
	/>
);

export default RedirectOnLoginRoute;
