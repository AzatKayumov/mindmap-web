import React, {Component} from 'react';
import {Switch, Route, Redirect, withRouter} from 'react-router-dom';

const PrivateRoute = ({component: Component, isLoggedIn, isLoginProcessing, redirectTo = '/login', withParams, ...rest}) => (
	<Route
		{...rest}
		render={props => {
			if (withParams) {
				redirectTo += rest.location.search;
			}

			if (!isLoggedIn && !isLoginProcessing) {
				return (
					<Redirect
						to={{
							pathname: redirectTo,
							state: {from: props.location}
						}}
					/>
				);
			}

			return (
				<Component {...props} />
			);
		}}
	/>
);

export default PrivateRoute;
