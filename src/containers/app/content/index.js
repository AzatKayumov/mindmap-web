import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Switch, Route, Redirect, withRouter} from 'react-router-dom';

import HomePage from "containers/home";
import MapPage from "containers/map";



import style from './style.scss';

class Content extends Component {
    render() {
        return (
            <div className={style.content}>
                <Switch>
                    <Route
                        exact
                        path="/"
						component={MapPage}
                    />
                </Switch>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Content));
