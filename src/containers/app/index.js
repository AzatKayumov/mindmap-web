import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {ConnectedRouter} from 'connected-react-router';
import {Provider} from 'react-redux';

import Content from './content';

class App extends Component {
	render() {
		const {store, history} = this.props;
		return (
			<Provider store={store}>
				<ConnectedRouter history={history}>
					<Content store={store}/>
				</ConnectedRouter>
			</Provider>
		);
	}
}

App.propTypes = {
	store: PropTypes.any.isRequired,
	history: PropTypes.any.isRequired
};

export default App;
