import React from 'react';
import {withRouter} from "react-router";

import style from './style.scss';
import Header from "./header";

class HomePage extends React.Component {
    _handleGoBack = () => {
        this.props.history.push('/map/1');
    };

    render() {
        return (
            <div className={style.homePage}>
                <Header
                    onGoBack={this._handleGoBack}
                />
                <div className={style.content}>
                    <h1>Browse</h1>
                </div>
            </div>
        );
    }
}


export default withRouter(HomePage);