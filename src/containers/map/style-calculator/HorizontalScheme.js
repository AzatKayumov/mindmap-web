class HorizontalScheme {
	constructor(objectModel, verticalMargin = 15, horizontalMargin = 45) {
		this.verticalMargin = verticalMargin;
		this.horizontalMargin = horizontalMargin;
		this.objectModel = objectModel;
	}

	getNodeById = (nodeId) => {
		return this.objectModel.getNodeById(nodeId);
	};

	getConnectionById = (connectionId) => {
		return this.objectModel.getConnectionById(connectionId);
	};

	_computeBoundRects = () => {
		this.objectModel.walkTree(null, (nodeMeta, parentNodeMeta) => {

			const {id: nodeId, level, position, children} = nodeMeta;
			const node = this.getNodeById(nodeId);

			node._nodeSchema = node._nodeSchema || {};

			const nodeSchema = node._nodeSchema;
			nodeSchema.boundRect = nodeSchema.boundRect || {
				height: node._computed.height,
				width: node._computed.width
			};

			const {boundRect} = nodeSchema;
			boundRect.height = Math.max(node._computed.height, boundRect.height);
			boundRect.width += children.length > 0 ? node._computed.width + this.horizontalMargin : 0;

			if (parentNodeMeta) {
				const parentNode = this.getNodeById(parentNodeMeta.id);
				if (!parentNode._nodeSchema) {
					parentNode._nodeSchema = {
						boundRect: {}
					};
				}

				const {boundRect: parentBoundRect} = parentNode._nodeSchema;

				if (position === 0) {
					parentBoundRect.height = nodeSchema.boundRect.height;
					parentBoundRect.width = nodeSchema.boundRect.width;
				} else {
					parentBoundRect.height += this.verticalMargin + nodeSchema.boundRect.height;
					parentBoundRect.width = Math.max(parentBoundRect.width, parentNode._computed.width + this.horizontalMargin + nodeSchema.boundRect.width);
				}
			}


			if (children.length === 0) {
				nodeSchema.offsetY = node._computed / 2;

			} else if (children.length === 1) {
				nodeSchema.offsetY = boundRect.height / 2;
			} else {
				const firstChildMeta = children[0];
				const lastChildMeta = children[children.length - 1];

				const firstChild = this.getNodeById(firstChildMeta.id);
				const lastChild = this.getNodeById(lastChildMeta.id);

				const grip = (boundRect.height - firstChild._computed.height / 2 - lastChild._computed.height / 2);
				nodeSchema.offsetY = firstChild._computed.height / 2 + grip / 2;
			}

			nodeSchema.parentGroupOffsetY = 0;
			if (position === 1) {
				nodeSchema.parentGroupOffsetY = node._computed.height/2;
			} else if (position > 0) {
				const prevNodeMeta = parentNodeMeta.children[position - 1];
				const prevNode = this.getNodeById(prevNodeMeta.id);

				nodeSchema.parentGroupOffsetY = prevNode._nodeSchema.parentGroupOffsetY;
				nodeSchema.parentGroupOffsetY += prevNode._nodeSchema.boundRect.height;
				nodeSchema.parentGroupOffsetY += this.verticalMargin;
			}
			//
			// if (position === 0) {
			//
			// 	if (children.length > 0) {
			// 		nodeSchema.parentGroupOffsetY = nodeSchema.offsetY;
			// 	}
			//
			// } else {
			//
			// 	nodeSchema.parentGroupOffsetY = 0;
			// 	if (node._computed.height < nodeSchema.boundRect.height) {
			//
			// 		nodeSchema.parentGroupOffsetY = nodeSchema.boundRect.height / 2 - node._computed.height / 2;
			// 	}
			// }

			// let childrenHeight = 0;
			// let childrenWidth = 0;
			//
			// children.forEach(childMeta => {
			// 	const child = this.getNodeById(childMeta.id);
			//
			// 	if (childMeta.position > 0) {
			// 		childrenHeight += this.verticalMargin;
			// 	}
			//
			// 	childrenHeight += child._nodeSchema.boundRect.height;
			// 	childrenWidth = Math.max(childrenWidth, child._nodeSchema.boundRect.width);
			// });
			//
			// const boundRect = {
			// 	width: node._computed.width + (childrenWidth > 0 ? this.horizontalMargin : 0) + childrenWidth,
			// 	height: Math.max(node._computed.height, childrenHeight)
			// };
			//
			// let offsetY = 0;
			//
			// if (children.length === 1) {
			//
			// 	offsetY = boundRect.height / 2 - node._computed.height / 2;
			//
			// } else if (children.length > 1) {
			//
			// 	const firstChildMeta = children[0];
			// 	const lastChildMeta = children[children.length - 1];
			//
			// 	const firstChild = this.getNodeById(firstChildMeta.id);
			// 	const lastChild = this.getNodeById(lastChildMeta.id);
			//
			// 	const grip = (boundRect.height - firstChild._computed.height / 2 - lastChild._computed.height / 2);
			// 	offsetY = firstChild._computed.height / 2 + grip / 2 - node._computed.height / 2;
			// }
			//
			// node._nodeSchema = {
			// 	boundRect,
			// 	offsetY
			// };
		});
	};

	_computePositions = () => {

		this._computeBoundRects();

		let rootGroupY = 0;

		this.objectModel.walkTree((nodeMeta, parentNodeMeta) => {

			const {id: nodeId, level, position, children} = nodeMeta;
			const node = this.getNodeById(nodeId);

			if (level === 0) {
				rootGroupY = node._computed.y - node._nodeSchema.offsetY;
				return;
			}

			const parentNode = this.getNodeById(parentNodeMeta.id);
			node._computed.x = parentNode._computed.x + parentNode._computed.width + this.horizontalMargin;
			node._computed.y = parentNode._computed.y - parentNode._nodeSchema.offsetY + node._nodeSchema.parentGroupOffsetY;
		});
	};

	_computeConnections = () => {
		this.objectModel.walkTree((nodeMeta, parentNodeMeta) => {
			const node = this.getNodeById(nodeMeta.id);

			nodeMeta.childrenConnectionsId.forEach((connectionId) => {
				const connection = this.getConnectionById(connectionId);

				connection._computed = {
					...connection._computed,
					sourceData: {
						...node._computed,
						nodeMeta: {...nodeMeta},
						parentNodeMeta: {...parentNodeMeta}
					},
				};
			});

			if (nodeMeta.parentConnectionId) {
				const connection = this.getConnectionById(nodeMeta.parentConnectionId);

				connection._computed = {
					...connection._computed,
					targetData: {
						...node._computed,
						nodeMeta: {...nodeMeta},
						parentNodeMeta: {...parentNodeMeta}
					},
				};
			}
		});
	};

	compute = () => {
		this._computePositions();
		this._computeConnections();
	};
}

export default HorizontalScheme;
