class VerticalScheme {
	constructor(objectModel, verticalMargin = 30, horizontalMargin = 40) {
		this.verticalMargin = verticalMargin;
		this.horizontalMargin = horizontalMargin;
		this.objectModel = objectModel;
	}

	getNodeById = (nodeId) => {
		return this.objectModel.getNodeById(nodeId);
	};

	getConnectionById = (connectionId) => {
		return this.objectModel.getConnectionById(connectionId);
	};

	_computePositions = () => {

		this.objectModel.walkTree((nodeMeta, parentNodeMeta) => {

			const {level, position} = nodeMeta;

			if (level === 0) {
				return;
			}

			const node = this.getNodeById(nodeMeta.id);
			const parentNode = this.getNodeById(nodeMeta.parentId);

			node._computed.x = parentNode._computed.x + this.horizontalMargin;


			if (position === 0) {

				node._computed.y = parentNode._computed.y + parentNode._computed.height + this.verticalMargin;

			} else {

				const prevNodeMeta = parentNodeMeta.children[position - 1];
				const prevNode = this.getNodeById(prevNodeMeta.id);
				node._computed.y = prevNode._computed.y + prevNode._computed.height + this.verticalMargin;

				node._computed.y += prevNode._nodeSchema.childrenHeight;

				if (prevNode._nodeSchema.childrenHeight > 0) {
					node._computed.y += this.verticalMargin;
				}
			}

		}, (nodeMeta, parentNodeMeta) => {

			const {level, position, children} = nodeMeta;
			const node = this.getNodeById(nodeMeta.id);

			if (level === 0) {
				return;
			}

			let childrenHeight = 0;

			children.forEach((childMeta, index) => {
				const child = this.getNodeById(childMeta.id);

				childrenHeight += child._computed.height;

				if (index < children.length - 1) {
					childrenHeight += this.verticalMargin;
				}

				childrenHeight += child._nodeSchema.childrenHeight;

				if (child._nodeSchema.childrenHeight > 0) {
					childrenHeight += this.verticalMargin;
				}
			});

			node._nodeSchema = {
				childrenHeight
			};
		});
	};

	_computeConnections = () => {
		this.objectModel.walkTree((nodeMeta, parentNodeMeta) => {
			const node = this.getNodeById(nodeMeta.id);

			nodeMeta.childrenConnectionsId.forEach((connectionId) => {
				const connection = this.getConnectionById(connectionId);

				connection._computed = {
					...connection._computed,
					sourceData: {
						...node._computed,
						nodeMeta: {...nodeMeta},
						parentNodeMeta: {...parentNodeMeta}
					},
				};
			});

			if (nodeMeta.parentConnectionId) {
				const connection = this.getConnectionById(nodeMeta.parentConnectionId);

				connection._computed = {
					...connection._computed,
					targetData: {
						...node._computed,
						nodeMeta: {...nodeMeta},
						parentNodeMeta: {...parentNodeMeta}
					},
				};
			}
		});
	};

	compute = () => {
		this._computePositions();
		this._computeConnections();
	};
}

export default VerticalScheme;
