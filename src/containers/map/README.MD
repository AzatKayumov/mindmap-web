Connections schemes:

#DOM
```javascript
const DOM = {
	schemeVersion: '12',
	id: 'uuid',
	classes: {
		
	},
	board: {
		x: 0,
		y: 0,
		
		class: '',
		style: {
			//Contains user defined styles
		},
		_computed: {}//Styles calculated from class template and user input (don't serialize) and
	},

	rootNode: [
		{
			id: 'uuid',
			text: "NODE TEXT",
			class: 'myClass', //Style class from template,
			style: { 
				//Contains user defined styles
				x: 0, //Node position,
				y: 0, //Node position,
				width: 250, //user specified width,
				connectionScheme: 1, // One of connections scheme,
			},
			
			_computed: {}, //Styles calculated from class template and user input (don't serialize) and,
			
			connection: {
				class: 'myClass',
				style: {},
				_computed: {}
			},
			children: [
				//...other nodes
			]
		},
	],
	
	connections: [
		{
			id: 'uuid',
			source: 'uuid', //source node
			sourcePointPosition: 0, //Position of connection point on scheme,
			
			target: 'uuid', //target node,
			targetPointPosition: 1, //Position of point on scheme,
			
			class: 'myClass', //Style class from template,
			style: { 
				//Contains user defined styles
			},

			
			_computed: {} //Styles calculated from class template and user input (don't serialize)
		}
	]
};
```


#TEMPLATE FILE
```javascript
const TEMPLATE = {
   "boardBackground": "#0a020e",
   "selectionColor": "blue",
   "selectionWidth": 2,
   "connectionColors": [
	"#CD5B52",
	"#EDB634",
	"#66C75E",
	"#7285D4",
	"#582790",
	"#963E84"
   ],
   "classes": {
	"centralTopic": {
	  "shape": "roundedRect",
	  "background": "#1F173B",
	  "borderWidth": 5,
	  "borderColor": "#4877b1",
	  "borderRadius": 5,
	  "color": "white",
	  "fontSize": 30,
	  "connectionScheme": 1,
	  "connectionShape": "line",
	  "connectionWidth": 3,
	  "connectionColor": "red",
	  "connectionStyle": "solid",
	  "minWidth": 200,
	  "padding": 10
	},
	"centralTopicConnection": {
	  "shape": "line",
	  "width": 3,
	  "color": "red",
	  "style": "solid"
	},
	"secondTopic": {
	  "shape": "roundedRect",
	  "background": "#1F173B",
	  "borderWidth": 1,
	  "borderColor": "red",
	  "borderRadius": 5,
	  "color": "red",
	  "fontSize": 20,
	  "minWidth": 200,
	  "connectionScheme": 1,
	  "padding": 10
	},
	"secondTopicConnection": {
	  "shape": "line",
	  "width": 3,
	  "color": "red",
	  "style": "solid"
	}
   },
   "levels": [
	{
	  "node": {
		"class": "centralTopic"
	  },
	  "connection": {
		"class": "centralTopicConnection"
	  }
	},
	{
	  "node": {
		"class": "secondTopic"
	  },
	  "connection": {
		"class": "secondTopicConnection"
	  }
	}
   ]
 }
```
