import React from 'react';
import PropTypes from 'prop-types';

import style from './style.scss';

import BackButton from 'assets/icons/back.svg';

class Header extends React.Component {
	render() {
		const {onSave} = this.props;
		return (
			<header className={style.header}>
				<BackButton
					className={style.backButton}
				/>
				<h1 className={style.title}>Mind Map</h1>
				<button
					className={style.viewButton}
					onClick={onSave}
				>
					Save
				</button>
				<button
					className={style.viewButton}
				>
					View
				</button>
			</header>
		);
	}
}

Header.propTypes = {
	onSave: PropTypes.func.isRequired
};

Header.defaultProps = {};

export default Header;
