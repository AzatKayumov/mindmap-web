import React from 'react';
import PropTypes from 'prop-types';

import {Stage as KonvaStage, Rect} from 'react-konva';
import {getDistance} from "../../../utils/graph";

function clientPointerRelativeToStage(clientX, clientY, stage) {
	return {
		x: clientX - stage.getContent().offsetLeft,
		y: clientY - stage.getContent().offsetTop,
	};
}

class Stage extends React.Component {

	constructor(props) {
		super(props);
		this._lastDist = 0;
	}

	_handleMouseWheel = (e) => {
		e.evt.preventDefault();

		const scaleBy = 1.1;
		const stage = e.target.getStage();
		const oldScale = stage.scaleX();
		const mousePointTo = {
			x: stage.getPointerPosition().x / oldScale - stage.x() / oldScale,
			y: stage.getPointerPosition().y / oldScale - stage.y() / oldScale
		};

		const newScale = e.evt.deltaY > 0 ? oldScale / scaleBy : oldScale * scaleBy;

		stage.scale({x: newScale, y: newScale});

		const offsetX = -(mousePointTo.x - stage.getPointerPosition().x / newScale) * newScale;
		const offsetY = -(mousePointTo.y - stage.getPointerPosition().y / newScale) * newScale;

		this.props.onChange({
			offsetX,
			offsetY,
			zoom: newScale
		});
	};

	_handleTouchMove = (evt) => {

		const t1 = evt.touches[0];
		const t2 = evt.touches[1];

		if (t1 && t2) {
			evt.preventDefault();
			evt.stopPropagation();

			const oldScale = this._stage.scaleX();

			const dist = getDistance(
				{x: t1.clientX, y: t1.clientY},
				{x: t2.clientX, y: t2.clientY}
			);
			if (!this._lastDist) this._lastDist = dist;
			const delta = dist - this._lastDist;

			const px = (t1.clientX + t2.clientX) / 2;
			const py = (t1.clientY + t2.clientY) / 2;
			const pointer = this._point || clientPointerRelativeToStage(px, py, this._stage);
			if (!this._point) this._point = pointer;

			const startPos = {
				x: pointer.x / oldScale - this._stage.x() / oldScale,
				y: pointer.y / oldScale - this._stage.y() / oldScale,
			};

			const scaleBy = 1.01 + Math.abs(delta) / 100;
			const newScale = delta < 0 ? oldScale / scaleBy : oldScale * scaleBy;
			this._stage.scale({x: newScale, y: newScale});

			const newPosition = {
				x: (pointer.x / newScale - startPos.x) * newScale,
				y: (pointer.y / newScale - startPos.y) * newScale,
			};

			// stage.position(newPosition);
			// stage.batchDraw();
			this.props.onChange({
				offsetX: newPosition.x,
				offsetY: newPosition.y,
				zoom: newScale,
			});

			this._lastDist = dist;
		}
	};

	_handleTouchEnd = () => {
		this._lastDist = 0;
		this._point = undefined;
	};

	_handleSetStageRef = (stage) => {

		this._stage = stage;
		window.stage = stage;
		if (!stage) {
			return;
		}

		const content = stage.getContent();
		content.addEventListener('touchmove', this._handleTouchMove, false);
		content.addEventListener('touchend', this._handleTouchEnd, false);
	};

	_handleDragEnd = (e) => {
		if (e.target !== this._stage) {
			return;
		}

		const position = this._stage.getPosition();

		this.props.onChange({
			offsetX: position.x,
			offsetY: position.y
		});
	};

	render() {
		const {x, y, zoom, children, ...rest} = this.props;
		return (
			<KonvaStage
				{...rest}
				scale={zoom}
				draggable={true}
				onWheel={this._handleMouseWheel}
				x={x}
				y={y}
				ref={this._handleSetStageRef}
				// onClick={this._handleFocusNode(null)}
				onDragEnd={this._handleDragEnd}
				onDragMove={this._handleDragMove}
			>
				{children}
			</KonvaStage>
		);
	}
}

Stage.propTypes = {
	zoom: PropTypes.number,
	x: PropTypes.number,
	y: PropTypes.number,
	onChange: PropTypes.func.isRequired
};

Stage.defaultProps = {};

export default Stage;
