import uuid from "uuid";

import Node from './node';
import VerticalScheme from "containers/map/style-calculator/VerticalScheme";
import HorizontalScheme from "containers/map/style-calculator/HorizontalScheme";

class ObjectModel {
	constructor(styleTemplate) {
		this.nodes = [];
		this.connections = [];
		this.boardOffsetX = 0;
		this.boardOffsetY = 0;
		this.boardZoom = 1;
		this.boardBackground = 'white';
		this._metaInfoTree = null; //read only tree
		this._metaInfoById = null; //read only tree
		this._styleTemplate = styleTemplate;
	}

	static unserialize(serializedData, styleTemplate) {
		const newModel = new ObjectModel(styleTemplate);
		const {boardOffsetX, boardOffsetY, boardZoom, nodes, connections} = serializedData;

		newModel.boardOffsetX = boardOffsetX;
		newModel.boardOffsetY = boardOffsetY;
		newModel.boardZoom = boardZoom;
		newModel.nodes = nodes.map(node => ({
			...node,
			_computed: {},
		}));
		newModel.boardBackground = styleTemplate.boardBackground;
		newModel.connections = connections.map(connection => ({
			...connection,
			_computed: {},
		}));
		newModel._updateMetaInfo();

		newModel._compute();
		const vert = new HorizontalScheme(newModel);
		vert.compute();

		return newModel;
	}

	serialize = () => {
		const data = {
			boardZoom: this.boardZoom,
			boardOffsetX: this.boardOffsetX,
			boardOffsetY: this.boardOffsetY,
			nodes: this.nodes.map(node => ({
				id: node.id,
				text: node.text,
				class: node.class,
				style: node.style
			})),
			connections: this.connections.map(connection => ({
				id: connection.id,
				position: connection.position,
				source: connection.source,
				target: connection.target,
				class: connection.class,
				style: connection.style
			}))
		};

		return JSON.stringify(data, null, 2);
	};

	copy = (objectFieldsToCopy = ['nodes', 'connections']) => {

		const modelCopy = new ObjectModel();

		modelCopy.nodes = this.nodes;
		modelCopy.connections = this.connections;
		modelCopy.boardOffsetX = this.boardOffsetX;
		modelCopy.boardOffsetY = this.boardOffsetY;
		modelCopy.boardZoom = this.boardZoom;
		modelCopy.boardBackground = this.boardBackground;
		modelCopy._styleTemplate = this._styleTemplate;
		modelCopy._metaInfoTree = this._metaInfoTree;
		modelCopy._metaInfoById = this._metaInfoById;

		// objectFieldsToCopy.forEach(field => {
		// 	const currentValue = this[field];
		//
		// 	if (Array.isArray(currentValue)) {
		// 		modelCopy[field] = currentValue.slice();
		// 	}
		// });

		return modelCopy;
	};

	getNodeById = (nodeId) => {
		return this.nodes.find(node => node.id === nodeId);
	};

	getMetaById = (nodeId) => {
		return this._metaInfoById[nodeId];
	};

	getConnectionById = (connectionId) => {
		return this.connections.find(connection => connection.id === connectionId);
	};

	getClassData = (className) => {
		return this._styleTemplate.classes[className];
	};

	addNode = (parentNodeId, text) => {

		const parentNode = this.getNodeById(parentNodeId);
		const parenNodeMeta = this.getMetaById(parentNodeId);

		const parentNodeClass = parentNode.class;
		const parentNodeClassData = this.getClassData(parentNodeClass);
		const connectionClass = parentNodeClassData.connectionClass;

		const newNodeId = uuid();
		let newNode = {
			text,
			id: newNodeId,
			class: parentNodeClassData.childNodeClass,
			style: {},
			_computed: {}
		};

		newNode = this._computeStyle(newNode);
		newNode = ObjectModel._computeNodeSize(newNode);

		let newConnection = {
			id: uuid(),
			position: parenNodeMeta.children.length,
			source: parentNodeId,
			target: newNode.id,
			class: connectionClass,
			style: {},
			_computed: {}
		};

		newConnection = this._computeStyle(newConnection);

		const newObjectModel = this.copy();

		newObjectModel.nodes = this.nodes.slice();
		newObjectModel.nodes.push(newNode);

		newObjectModel.connections = this.connections.slice();
		newObjectModel.connections.push(newConnection);
		newObjectModel._updateMetaInfo();

		const vert = new HorizontalScheme(newObjectModel);
		vert.compute();

		return {
			newObjectModel,
			newNodeId
		};
	};

	static _computeNodeSize(node) {
		const size = new Node({data: node}).getSize();
		return {
			...node,
			_computed: {
				...node._computed,
				...size
			}
		};
	}

	_computeStyle(item) {
		const classData = this.getClassData(item.class);
		return {
			...item,
			_computed: {
				x: 0,
				y: 0,
				...classData,
				...(item.style || {})
			}
		};
	}

	_updateMetaInfo = () => {
		const metaInfoById = {};
		const updateMetaRecursive = function (connections, nodes, nodeId, level, position) {
			//FIXME: optimize
			const connection = connections.find(con => con.target === nodeId);
			const children = connections.filter(con => con.source === nodeId)
				.sort((a, b) => {
					if (a.position === b.position) {
						return 0;
					}

					return a.position > b.position ? 1 : -1;
				})
				.map((connection, position) => updateMetaRecursive(connections, nodes, connection.target, level + 1, position));

			const nodeMetaInfo = {
				id: nodeId,
				parentId: connection && connection.source,
				level,
				position,
				parentConnectionId: connection && connection.id,
				childrenConnectionsId: children.map(child => child.parentConnectionId),
				children
			};

			metaInfoById[nodeId] = nodeMetaInfo;
			return nodeMetaInfo;
		};

		const rootNode = this.nodes.find(node => this.connections.every(connection => connection.target !== node.id));
		this._metaInfoTree = updateMetaRecursive(this.connections, this.nodes, rootNode.id, 0, 0);
		this._metaInfoById = metaInfoById;
	};

	walkTree = (callbackBefore, callbackAfter) => {
		this._walkTree(callbackBefore, callbackAfter, this._metaInfoTree, null);
	};

	_walkTree = (callbackBefore, callbackAfter, node, parentNode) => {
		if (callbackBefore && callbackBefore(node, parentNode)) {
			return true;
		}

		for (let i = 0; i < node.children.length; i++) {
			const childNode = node.children[i];

			const stop = this._walkTree(callbackBefore, callbackAfter, childNode, node);
			if (stop) {
				return true;
			}
		}

		return callbackAfter && callbackAfter(node, parentNode);
	};


	_compute = () => {
		//FIXME OPTIMIZE
		for (let i = 0; i < this.nodes.length; i++) {
			let node = this.nodes[i];
			node = this._computeStyle(node);
			this.nodes[i] = ObjectModel._computeNodeSize(node);
		}

		for (let i = 0; i < this.connections.length; i++) {
			let connection = this.connections[i];
			this.connections[i] = this._computeStyle(connection);
		}
	};

	moveNode = (nodeId, {x, y}) => {
		const newObjectModel = this.copy();
		newObjectModel.nodes = newObjectModel.nodes.map(node => ({
			...node,
			style: {
				...node.style,
				x,
				y
			},
			_computed: {
				...node._computed,
				x,
				y
			}
		}));

		return newObjectModel;
	};

	changeBoardConfig = ({boardZoom, boardOffsetX, boardOffsetY}) => {
		const newObjectModel = this.copy();
		if (boardOffsetX !== undefined) {
			newObjectModel.boardOffsetX = boardOffsetX;
		}

		if (boardOffsetX !== undefined) {
			newObjectModel.boardOffsetY = boardOffsetY;
		}

		if (boardZoom !== undefined) {
			newObjectModel.boardZoom = boardZoom;
		}

		newObjectModel.nodes = newObjectModel.nodes.slice();
		return newObjectModel;
	};

	changeNodeProperty = (nodeId, changer) => {

		const node = this.getNodeById(nodeId);
		const changedNode = changer(node);

		const newObjectModel = this.copy();

		newObjectModel.nodes = newObjectModel.nodes.map(item => {
			if (item.id !== nodeId) {
				return item;
			}

			return changedNode;
		});

		newObjectModel._compute();
		const vert = new HorizontalScheme(newObjectModel);
		vert.compute();

		return newObjectModel;
	};

	getRootNode = () => {
	    return this.getNodeById(this._metaInfoTree.id);
	};
}

export default ObjectModel;
