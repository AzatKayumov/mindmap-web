import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from "react-dom";

import style from './style.scss';
import Konva from 'konva';
import {Group, Rect, Text, Circle} from "react-konva";
import Img from "components/image";
import Portal from "components/portal";

import addIcon from 'assets/icons/plus.png';


export function smooth(val) {
	if (val % 1 === 0) return val + 0.5;
	return val;
}

const SELECTION_PADDING = 8;
const SELECTION_BORDER_WIDTH = 1;


export const PointPosition = {
	TOP: 0,
	RIGHT: 1,
	BOTTOM: 2,
	LEFT: 3
};

class Node extends React.Component {
	constructor(props) {
		super(props);

		const {text, _computed} = props.data;
		const {x, y} = _computed;
		this.state = {
			text,
			textSize: Node._getTextSize({
				...props.data,
				text
			}),
			absolutePosition: {
				x: x - SELECTION_PADDING,
				y: y - SELECTION_PADDING
			}
		};
	}

	componentWillReceiveProps(nextProps) {
		const currentProps = this.props;

		if (nextProps.scale !== currentProps.scale ||
			nextProps.data !== currentProps.data) {

			const text = nextProps.data.text;
			this.setState({
				text,
				textSize: Node._getTextSize({
					...nextProps.data,
					text
				})
			});
		}
	}

	componentDidMount(prevProps) {
		if (this.props.isInEditMode) {
			setTimeout(() => {
				const element = document.getElementById(`edit-node-${this.props.id}`);
				if (element) {
					element && element.focus();
					element && element.select();
				}

			}, 100);
		}
	}

	getSize = () => {
		const {textSize} = this.state;
		return textSize;
	};

	_handleAddNode = () => {
		this.props.onAddNode(this);
	};

	_handleDragMove = (e) => {
		e.evt.stopPropagation();
		e.evt.preventDefault();

		if (this.props.onDragMove) {
			const position = e.target.getPosition();

			this.props.onDragMove({
				x: position.x + SELECTION_PADDING,
				y: position.y + SELECTION_PADDING,
			});
		}
	};

	_handleDragEnd = (e) => {
		e.evt.stopPropagation();
		e.evt.preventDefault();

		if (this.props.onDragEnd) {
			const position = e.target.getPosition();

			this.props.onDragEnd({
				x: position.x + SELECTION_PADDING,
				y: position.y + SELECTION_PADDING,
			});
		}
	};

	_handleDragStart = (e) => {
		e.evt.stopPropagation();
		e.evt.preventDefault();

		if (this.props.onDragStart) {
			const position = e.target.getAbsolutePosition();

			this.props.onDragStart({
				x: position.x + SELECTION_PADDING,
				y: position.y + SELECTION_PADDING,
			});
		}
	};

	_handleChangeText = (e) => {
		const text = e.target.value;
		this.setState({
			text,
			textSize: Node._getTextSize({
				...this.props.data,
				text
			})
		});
	};

	_handleInputKeyPress = (e) => {
		const text = e.target.value;
		this.setState({
			text,
			textSize: Node._getTextSize({
				...this.props.data,
				text
			})
		});
	};

	static _getTextSize = function (data) {
		const {text} = data;
		const {fontSize, maxWidth, minWidth, minHeight, padding} = data._computed;
		const config = {
			text,
			fontSize,
			padding
		};

		const getTextSize = function (props = {}) {
			return new Konva.Text({...config, ...props}).getClientRect();
		};

		let textSize = getTextSize();

		if (minWidth && textSize.width < minWidth) {
			textSize = getTextSize({width: minWidth});
		}

		if (maxWidth && textSize.width > maxWidth) {
			textSize = getTextSize({width: maxWidth});
		}

		const height = textSize.height;
		return {
			width: textSize.width,
			height: !minHeight ? height : (height > minHeight ? height : minHeight)
		};
	};

	static _getSelectionRectSize = function (textSize) {
		return {
			width: textSize.width + 2 * SELECTION_PADDING,
			height: textSize.height + 2 * SELECTION_PADDING,
		};
	};

	static getConnectionPoint = function (pointPosition, nodeData) {
		const size = Node._getTextSize(nodeData);

		switch (pointPosition) {
			case PointPosition.RIGHT:
				return {
					absolute: {
						x: nodeData.x + size.width,
						y: nodeData.y + size.height / 2
					},
					relative: {
						x: size.width,
						y: size.height / 2
					}
				};
			case PointPosition.LEFT:
				return {
					absolute: {
						x: nodeData.x,
						y: nodeData.y + size.height / 2
					},
					relative: {
						x: 0,
						y: size.height / 2
					}
				};
			case PointPosition.TOP:
				return {
					absolute: {
						x: nodeData.x + size.width / 2,
						y: nodeData.y
					},
					relative: {
						x: size.width / 2,
						y: 0
					}
				};
			case PointPosition.BOTTOM:
				return {
					absolute: {
						x: nodeData.x + size.width / 2,
						y: nodeData.y + size.height
					},
					relative: {
						x: size.width / 2,
						y: size.height
					}
				};
		}

	};


	_handleBlurTextArea = (e) => {
		const text = e.target.value;

		if (this.props.onChangeText) {
			this.props.onChangeText(text);
		}

		if (this.props.onCancelEdit) {
			this.props.onCancelEdit();
		}
	};

	render() {
		const {
			id,
			scale,
			data,
			isInEditMode,
			isFocused,
			onFocus,
			onEdit,
			isDraggable,
			boardOffsetX,
			boardOffsetY,
			boardZoom
		} = this.props;
		let {x, y, width, height} = data._computed;
		const {padding, fontSize, color, borderColor, borderWidth, borderRadius, background} = data._computed;

		const {text, textSize} = this.state;
		const nodeSize = this.getSize();
		const selectionRectSize = Node._getSelectionRectSize(textSize);

		const editY = y + boardOffsetY;
		const editX = x + boardOffsetX;

		return (
			<Group
				x={x - SELECTION_PADDING}
				y={y - SELECTION_PADDING}
				draggable={isDraggable}
				onDragMove={this._handleDragMove}
				onDragStart={this._handleDragStart}
				onDragEnd={this._handleDragEnd}
				ref={(el) => this._container = el}
			>
				{
					isFocused &&
					<Rect
						x={smooth(0)}
						y={smooth(0)}
						height={selectionRectSize.height}
						width={selectionRectSize.width}
						stroke={'blue'}
						strokeWidth={SELECTION_BORDER_WIDTH}
						fill={'transparent'}
						className={'selection-rect'}
					/>
				}
				<Group
					x={SELECTION_PADDING}
					y={SELECTION_PADDING}
					onClick={onFocus}
					onTap={onFocus}
					onDblClick={onEdit}
					onDblTap={onEdit}
				>
					{
						textSize !== null &&
						<Rect
							x={smooth(0)}
							y={smooth(0)}
							cornerRadius={borderRadius}
							height={textSize.height}
							width={textSize.width}
							stroke={borderColor}
							strokeWidth={borderWidth}
							fill={background}
							onDblClick={onEdit}
							onDblTap={onEdit}
						/>
					}
					<Text
						x={0}
						y={0}
						fontSize={fontSize}
						padding={0}
						width={textSize.width}
						text={`${data._computed.height}:${data._nodeSchema.boundRect.height}:${data._nodeSchema.boundRect.width}`}
						fill={'red'}
						align='left'
						verticalAlign='top'
						ref={(el) => this._text = el}


						lineHeight={1}
						fontFamily={'Arial'}
						fontWeight={'normal'}
						letterSpacing={0}
					/>
					<Text
						x={0}
						y={0}
						fontSize={fontSize}
						padding={padding}
						width={textSize.width}
						text={text}
						fill={isInEditMode ? 'transparent' : color}
						align='center'
						verticalAlign='middle'
						ref={(el) => this._text = el}


						lineHeight={1}
						fontFamily={'Arial'}
						fontWeight={'normal'}
						letterSpacing={0}
					/>
				</Group>
				{
					nodeSize &&
					isFocused &&
					<Img
						x={selectionRectSize.width + 10}
						y={textSize.height / 2}
						src={addIcon}
						width={20}
						height={20}
						onClick={this._handleAddNode}
						onTap={this._handleAddNode}
					/>
				}
				{
					isInEditMode &&
					<Portal
						isOpened={true}
					>
						<textarea
							id={`edit-node-${id}`}
							ref={(el) => {
								this._textArea = el;
							}}
							style={{
								position: 'absolute',
								top: editY,
								left: editX,
								zIndex: 1000,
								background: 'transparent',
								border: 'none',
								padding: padding,
								width,
								height,


								resize: 'none',
								outline: 'none',
								boxShadow: 'none',
								overflow: 'hidden',
								margin: 0,

								color: color,
								fontSize,
								textAlign: 'center',
								fontFamily: 'Arial',
								fontStyle: 'normal',
								lineHeight: 1,
							}}
							value={text}
							onChange={this._handleChangeText}
							onKeyPress={this._handleInputKeyPress}
							onBlur={this._handleBlurTextArea}
						/>
					</Portal>
				}
			</Group>
		);
	}
}

Node.propTypes = {
	isDraggable: PropTypes.bool,
	onDragMove: PropTypes.func,
	onDragStart: PropTypes.func,
	onDragEnd: PropTypes.func,
	isFocused: PropTypes.bool,
	onResize: PropTypes.func,
	onAddNode: PropTypes.func,
	onFocus: PropTypes.func,
	isInEditMode: PropTypes.bool,
	onEdit: PropTypes.func,
	onCancelEdit: PropTypes.func,
	onChangeText: PropTypes.func,
	data: PropTypes.shape({
		x: PropTypes.number,
		y: PropTypes.number,
		text: PropTypes.string,
		padding: PropTypes.number,
		borderRadius: PropTypes.number,
		borderColor: PropTypes.string,
		background: PropTypes.string,
		maxWidth: PropTypes.number,
	})
};

Node.defaultProps = {
	isDraggable: false,
};

export default Node;
