import React from 'react';
import PropTypes from 'prop-types';
import {withRouter} from "react-router";
import {Layer, Rect, Group} from "react-konva";
import Konva from 'konva';
import uuid from 'uuid/v4';

import Header from "./header";
import Node, {PointPosition} from "./node";
import Connection from './connection';
import Stage from './stage';

import testMap from '../../../test_map';
import styleTemplate from '../../../style_template_1';
import style from './style.scss';
import {saveTextAsFile} from "../../utils";
import ObjectModel from "containers/map/ObjectModel";


class MapPage extends React.Component {

	constructor(props) {
		super(props);

		this.objectModel = ObjectModel.unserialize(testMap, styleTemplate);
		this.state = {
			focusedItem: null,
			editingItem: null,
			objectModel: this.objectModel
		};
	}

	_handleGoBack = () => {
		this.props.history.push('/');
	};

	_handleFocusNode = (nodeId) => () => {
		this.setState({
			focusedItem: nodeId,
			editingItem: null
		});
	};

	_handleEditNode = (nodeId) => () => {
		this.setState({
			editingItem: nodeId,
			focusedItem: nodeId
		});
	};

	_handleCancelEditNode = (nodeId) => () => {
		this.setState({
			editingItem: null,
			focusedItem: nodeId
		});
	};

	_handleChangeNodeText = (nodeId) => (text) => {
		this.objectModel = this.objectModel.changeNodeProperty(nodeId, (node) => {
			return {
				...node,
				text
			};
		});

		this.setState({objectModel: this.objectModel});
	};

	_handleAddNode = (parentId) => () => {
		const {newObjectModel, newNodeId} = this.objectModel.addNode(parentId, 'Add text');
		this.objectModel = newObjectModel;
		this.setState({
			objectModel: this.objectModel,
			focusedItem: newNodeId,
			editingItem: newNodeId
		});
	};

	_handleMoveNode = (nodeId) => (newNodePosition) => {
		this.objectModel = this.objectModel.moveNode(nodeId, newNodePosition);
		this.setState({
			objectModel: this.objectModel
		});
	};

	_handleDragNodeStart = (nodeId) => ({x, y}) => {
		this._startNodePosition = {x, y};
	};

	_handleDragNodeEnd = (nodeId) => (newNodePosition) => {
		this._startNodePosition = null;

		this.objectModel = this.objectModel.moveNode(nodeId, newNodePosition);
		this.setState({
			objectModel: this.objectModel
		});
	};

	_onChangeStage = ({zoom, offsetX, offsetY}) => {
		this.objectModel = this.objectModel.changeBoardConfig({
			boardZoom: zoom,
			boardOffsetX: offsetX,
			boardOffsetY: offsetY
		});

		this.setState({
			objectModel: this.objectModel
		});
	};

	_handleSave = () => {
		const data = this.objectModel.serialize();
		saveTextAsFile('map.json', '.json', data);
	};

	render() {
		const {focusedItem, editingItem, objectModel} = this.state;
		const {nodes, connections, boardZoom, boardOffsetX, boardOffsetY, boardBackground} = objectModel;
		global.objectModel = objectModel;

		const rootNode = objectModel.getRootNode();
		const rootGroupY = rootNode._computed.y - rootNode._nodeSchema.offsetY;

		return (
			<div className={style.mapPage}>
				<Header
					onSave={this._handleSave}
					onGoBack={this._handleGoBack}
				/>
				<div
					id={'stage-container'}
					className={style.content}
					style={{background: boardBackground}}
				>
					<Stage
						width={window.innerWidth}
						height={window.innerHeight - 60}
						zoom={boardZoom}
						onChange={this._onChangeStage}
						x={boardOffsetX}
						y={boardOffsetY}
						// onClick={this._handleFocusNode(null)}
					>
						<Layer>

							<Rect
								x={rootNode._computed.x}
								y={rootGroupY}
								stroke={'red'}
								strokeWidth={1}
								height={rootNode._nodeSchema.boundRect.height}
								width={rootNode._nodeSchema.boundRect.width}
							/>
							{
								connections.map(connection => {

									if (!connection._computed) {
										return null;
									}

									return (
										<Connection
											key={connection.id}
											id={connection.id}
											data={connection}
										/>
									);
								})
							}
							{
								nodes.map(node => {
									if (!node._computed) {
										return null;
									}

									return (
										<Node
											key={node.id}
											id={node.id}
											data={node}
											scale={boardZoom}
											isDraggable={true}
											onDragStart={this._handleDragNodeStart(node.id)}
											onDragEnd={this._handleDragNodeEnd(node.id)}
											onDragMove={this._handleMoveNode(node.id)}
											onAddNode={this._handleAddNode(node.id)}
											isFocused={focusedItem === node.id}
											onFocus={this._handleFocusNode(node.id)}
											isInEditMode={editingItem === node.id}
											onEdit={this._handleEditNode(node.id)}
											onChangeText={this._handleChangeNodeText(node.id)}
											onCancelEdit={this._handleCancelEditNode(node.id)}
											boardOffsetX={boardOffsetX}
											boardOffsetY={boardOffsetY}
											boardZoom={boardZoom}
										/>
									);
								})
							}
						</Layer>
					</Stage>
				</div>
			</div>
		);
	}
}

export default withRouter(MapPage);
