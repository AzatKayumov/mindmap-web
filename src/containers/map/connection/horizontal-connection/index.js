import React from 'react';
import PropTypes from 'prop-types';
import {Shape} from 'react-konva';

import style from './style.scss';

class HorizontalConnection extends React.Component {
	render() {
		const margin = 20;
		const {data} = this.props;
		const {source, target, id} = data;
		const {width, color, sourceData, targetData} = data._computed;

		const direction = targetData.y - sourceData.y;


		const startPointX = sourceData.x + sourceData.width;
		const startPointY = sourceData.y + sourceData.height / 2;

		const endPointX = targetData.x;
		const endPointY = targetData.y + targetData.height / 2;

		if (direction === 0) {
			return (
				<Shape
					sceneFunc={(context, shape) => {
						context.beginPath();
						context.moveTo(startPointX, startPointY);
						context.lineTo(endPointX, endPointY);
						// context.closePath();
						// (!) Konva specific method, it is very important
						context.fillStrokeShape(shape);
					}}
					stroke={color}
					strokeWidth={width}
				/>
			);
		}

		if (direction > 0) {
			return (
				<Shape
					sceneFunc={(context, shape) => {
						context.beginPath();
						context.moveTo(startPointX, startPointY);
						context.lineTo(startPointX + margin, startPointY);
						context.lineTo(startPointX + margin, endPointY);
						context.lineTo(endPointX, endPointY);
						// context.closePath();
						// (!) Konva specific method, it is very important
						context.fillStrokeShape(shape);
					}}
					stroke={color}
					strokeWidth={width}
				/>
			);
		}

		return (
			<Shape
				sceneFunc={(context, shape) => {
					context.beginPath();
					context.moveTo(startPointX, startPointY);
					context.lineTo(startPointX + margin, startPointY);
					context.lineTo(startPointX + margin, endPointY);
					context.lineTo(endPointX, endPointY);
					// context.closePath();
					// (!) Konva specific method, it is very important
					context.fillStrokeShape(shape);
				}}
				stroke={color}
				strokeWidth={width}
			/>
		);
	}
}

HorizontalConnection.propTypes = {};

HorizontalConnection.defaultProps = {};

export default HorizontalConnection;
