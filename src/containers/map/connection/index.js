import React from 'react';
import PropTypes from 'prop-types';

import {Line, Shape} from 'react-konva';
import VerticalConnection from "containers/map/connection/vertical-connection";
import HorizontalConnection from "containers/map/connection/horizontal-connection";

class Connection extends React.Component {
	render() {
		const {data} = this.props;

		return (
			<HorizontalConnection
				data={data}
			/>
		);

		return (
			<VerticalConnection
				data={data}
			/>
		);

		const {sourcePoint, targetPoint, color, width} = data._computed;

		const sourceAbsolutePoint = sourcePoint.absolute;
		const targetAbsolutePoint = targetPoint.absolute;

		return (
			<Line
				points={[
					sourceAbsolutePoint.x,
					sourceAbsolutePoint.y,
					sourceAbsolutePoint.x + 40,
					sourceAbsolutePoint.y,
					targetAbsolutePoint.x - 40,
					targetAbsolutePoint.y,
					targetAbsolutePoint.x,
					targetAbsolutePoint.y
				]}
				lineJoin={'round'}
				stroke={color}
				strokeWidth={width}
				// tension={0}
				lineCap={'round'}
				bezier={true}
			/>
		);
	}
}

Connection.propTypes = {
	id: PropTypes.string.isRequired,
	data: PropTypes.any
};

Connection.defaultProps = {};

export default Connection;
