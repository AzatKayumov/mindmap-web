import React from 'react';
import PropTypes from 'prop-types';
import {Shape} from 'react-konva';

import style from './style.scss';

const OFFSET = 10;

class VerticalConnection extends React.Component {
	_getSourceConnectionPoint = (sourceData) => {
		return {
			x: sourceData.x + 10,
			y: sourceData.y + sourceData.height
		};
	};

	render() {
		const {data} = this.props;
		const {source, target, id} = data;
		const {width, color, sourceData, targetData} = data._computed;

		const mainLineX = sourceData.x + OFFSET;

		const startY = sourceData.y;

		const middleX = mainLineX;
		const middleY = targetData.y + targetData.height/2;

		const endX = targetData.x;
		const endY = targetData.y + targetData.height/2;


		const radius = 10;

		return (
			<Shape
				sceneFunc={(context, shape) => {
					context.beginPath();
					context.moveTo(mainLineX, startY);
					context.lineTo(middleX, middleY - radius);
					context.arc(middleX + radius, middleY - radius, radius, Math.PI, Math.PI/2, true);
					context.lineTo(endX, endY);
					// context.closePath();
					// (!) Konva specific method, it is very important
					context.fillStrokeShape(shape);
				}}
				stroke={color}
				strokeWidth={width}
			/>
		);
	}
}

VerticalConnection.propTypes = {};

VerticalConnection.defaultProps = {};

export default VerticalConnection;
