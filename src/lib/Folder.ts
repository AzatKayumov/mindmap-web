class Folder {
	id: string = '';
	parentId: string = '';
	createdAt: number = 0;
	updatedAt: number = 0;
	title: string = '';
	childrenFoldersOrder: Array<string> = [];
	childrenFoldersOrderVersion: number = 1;
	childrenTasksOrder: Array<string> = [];
	childrenTasksOrderVersion: number = 1;
	isDeleted: boolean = false;
	deletedAt: number|null = null;
	deletedFrom: string|null = null;
	version: number = 0;
	//
	// constructor(id: string, parentId: string, time: number, title: string) {
	// 	this.id = id;
	// 	this.parentId = parentId;
	// 	this.title = title;
	// 	this.createdAt = time;
	// 	this.updatedAt = time;
	// 	this.childrenFolders = [];
	// 	this.childrenTasks = [];
	// }
}

export default Folder;
