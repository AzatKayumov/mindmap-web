import uuid from 'uuid/v4';
import * as config from '../../config.json';

declare var global: any;

import {callPostMethod} from "./utils";
import Folder from "./Folder";

const ChildrenFoldersOrderEntity = 1;
const FolderPropertyEntity = 2;
const ChildrenTasksOrderEntity = 3;
const TaskPropertyEntity = 4;

class ApiClient {
	static SYNC_TIMEOUT = 5000;

	private uiStore: any;
	private localVersion: number = 0;

	constructor() {
	}

	private async _callApiMethod(endPoint, method, params, authToken?): Promise<any> {
		console.log(`REQUEST: ${method}`, params);
		const response = await callPostMethod(
			endPoint,
			{
				id: uuid(),
				method,
				params
			},
			authToken || localStorage.getItem('token')
		);
		console.log(`RESPONSE: ${method}`, {params, response});

		return response;
	};

	private _getNextLocalVersion = (): number => {
		let localVersionStr = localStorage.getItem('localVersion');
		let localVersion = localVersionStr ? Number(localVersionStr) : 0;
		localVersion += 1;
		localStorage.setItem('localVersion', String(localVersion));

		return localVersion;
	};

	async init(uiStore: any): Promise<void> {
		this.uiStore = uiStore;
	}

	dispatchEvent(event) {
		this.uiStore.dispatch(event);
	}

	loginByEmail = async (email, password) => {
		return await this._callApiMethod(
			'/public/registrator',
			'loginByEmail', {
				email,
				password
			});
	};

	signUpByEmail = async (email, password) => {
		return await this._callApiMethod(
			'/public/registrator',
			'signUpByEmail', {
				email,
				password
			});
	};

	async getFolder(folderId): Promise<Folder> {
		const {error, result} = await this._callApiMethod(
			'/private/http/sync',
			'getFolder', {
				folderId
			});

		if (error) {
			throw error;
		}

		const receivedFolder = result;
		return receivedFolder;
	};

	getChildrenItems = async (folderId) => {
		const {error, result} = await this._callApiMethod(
			'/private/http/sync',
			'getChildrenItems', {
				folderId
			});

		if (error) {
			throw error;
		}

		return result;
	};

	getUserData = async (authToken) => {
		const {error, result} = await this._callApiMethod(
			'/private/http/sync',
			'getUserData',
			{},
			authToken
		);

		if (error) {
			throw error;
		}

		return result;
	};

	uploadOperation = async (entityId, serverVersion, operation) => {
		const {error, result} = await this._callApiMethod(
			'/private/http/sync',
			'uploadOperation',
			{
				lastLocalServerVersions: [
					{
						entityId: entityId,
						version: serverVersion
					}
				],
				operation
			}
		);

		if (error) {
			throw error;
		}

		return result;
	};

	addFolder = async (parentId, id, position, title, parentChildrenFoldersOrderVersion) => {
		//FIXME: check if operation was added before
		const localVersion = this._getNextLocalVersion();
		const result = await this.uploadOperation(
			parentId,
			parentChildrenFoldersOrderVersion,
			{
				id: uuid(),
				method: "addFolder",
				time: Date.now(),
				entityVersion: localVersion,
				entity: ChildrenFoldersOrderEntity,
				entityId: [parentId],
				isDisabled: false,
				params: {
					parentId,
					position,
					id,
					title
				}
			}
		);
	};

	addTask = async (folderId, id, position, title, parentChildrenTasksOrderVersion) => {
		const localVersion = this._getNextLocalVersion();
		const result = await this.uploadOperation(
			folderId,
			parentChildrenTasksOrderVersion,
			{
				id: uuid(),
				method: "addTask",
				time: Date.now(),
				entityVersion: localVersion,
				entity: ChildrenFoldersOrderEntity,
				entityId: [folderId],
				isDisabled: false,
				params: {
					folderId,
					position,
					id,
					title
				}
			}
		);
	};

	removeFolder = async (parentId, id, position, parentChildrenFoldersOrderVersion) => {
		const localVersion = this._getNextLocalVersion();
		const result = await this.uploadOperation(
			parentId,
			parentChildrenFoldersOrderVersion,
			{
				id: uuid(),
				method: "deleteFolder",
				time: Date.now(),
				entityVersion: localVersion,
				entity: ChildrenFoldersOrderEntity,
				entityId: [parentId],
				isDisabled: false,
				params: {
					parentId,
					position,
					id
				}
			}
		);
	};

	removeTask = async (folderId, id, position, parentChildrenTasksOrderVersion) => {
		const localVersion = this._getNextLocalVersion();
		const result = await this.uploadOperation(
			folderId,
			parentChildrenTasksOrderVersion,
			{
				id: uuid(),
				method: "deleteTask",
				time: Date.now(),
				entityVersion: localVersion,
				entity: ChildrenFoldersOrderEntity,
				entityId: [folderId],
				isDisabled: false,
				params: {
					folderId,
					position,
					id
				}
			}
		);
	};

	completeTask = async (folderId, id, position, parentChildrenTasksOrderVersion) => {
		const localVersion = this._getNextLocalVersion();
		const result = await this.uploadOperation(
			folderId,
			parentChildrenTasksOrderVersion,
			{
				id: uuid(),
				method: "completeTask",
				time: Date.now(),
				entityVersion: localVersion,
				entity: ChildrenFoldersOrderEntity,
				entityId: [folderId],
				isDisabled: false,
				params: {
					folderId,
					position,
					id
				}
			}
		);
	};

	testCreateTasks = async (rootId) => {

		const NUMBER_OF_TASKS = 1000;
		const NUMBER_OF_FOLDERS = 1000;

		for (let j = 0; j < NUMBER_OF_FOLDERS; j++) {
			const parentId = uuid();
			const rootFolder = await this.getFolder(rootId);
			await this.addFolder(rootId, parentId, rootFolder.childrenFoldersOrder.length, 'Folder ' + j, rootFolder.childrenFoldersOrderVersion);


			for (let i = 0; i < NUMBER_OF_TASKS; i++) {

				const parentFolder = await this.getFolder(parentId);
				const {childrenTasksOrderVersion, childrenTasksOrder} = parentFolder;
				console.log('folder = ' + j + ' task = ' + i);
				console.time();
				await this.addTask(parentId, uuid(), childrenTasksOrder.length, 'Task ' + i, childrenTasksOrderVersion);
				console.timeEnd();
			}
		}
	};
}

global._API = new ApiClient();
export default global._API;
