import * as config from '../../config.json';

export async function callPostMethod(url: string, data: any, token?: string): Promise<any> {
	const headers = {
		'content-type': 'application/json'
	};

	if (token) {
		(<any>headers).Authorization = 'Bearer ' + token;
	}

	// Default options are marked with *
	return fetch((<any>config).apiEndPoint + url, {
		body: JSON.stringify(data), // must match 'Content-Type' header
		cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		credentials: 'same-origin', // include, same-origin, *omit
		headers,
		method: 'POST', // *GET, POST, PUT, DELETE, etc.
		mode: 'cors', // no-cors, cors, *same-origin
		redirect: 'follow', // manual, *follow, error
		referrer: 'no-referrer' // *client, no-referrer
	}).then(response => response.json());
}
