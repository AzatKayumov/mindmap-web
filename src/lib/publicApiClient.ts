import uuid from "uuid/v4";
import {callPostMethod} from "./utils";

class PublicApiClient {

	private static async _callApiMethod(endPoint, method, params): Promise<any> {
		return await callPostMethod(endPoint, {
			id: uuid(),
			method,
			params
		});
	};

	async loginByEmail(email, password): Promise<{ error: any | null, result: { jwtToken: string } }> {
		return await PublicApiClient._callApiMethod(
			'/public/registrator',
			'loginByEmail', {
				email,
				password
			});
	}

	async signUpByEmail(email, password): Promise<{ error: any | null, result: { jwtToken: string } }> {
		return await PublicApiClient._callApiMethod(
			'/public/registrator',
			'signUpByEmail', {
				email,
				password
			});
	}
}

export default new PublicApiClient();
