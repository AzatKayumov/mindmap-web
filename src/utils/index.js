function addDispatchKeys(dispatch, mapper, creators, dispatchMapConfigs) {

	for (let j = 0; j < dispatchMapConfigs.length; j++) {
		const dispatchMapConfig = dispatchMapConfigs[j];
		let methodName;
		let method;

		if (typeof dispatchMapConfig === 'string') {
			methodName = dispatchMapConfig;
			method = creators[methodName];
		} else {
			methodName = dispatchMapConfig.key;
			method = dispatchMapConfig.method;
		}

		mapper[methodName] = function (...data) {
			return dispatch(method(...data));
		};
	}
}

export function createDispatchMapper() {
	const args = arguments;
	return function (dispatch) {
		const mapper = {};
		for (let i = 0; i < args.length; i += 2) {
			const creators = args[i];
			const mapConfig = args[i + 1];

			addDispatchKeys(dispatch, mapper, creators, mapConfig);
		}

		return mapper;
	};
}


export function filterProps(data, fields) {
	const result = {};
	for (let i = 0; i < fields.length; i++) {
		const field = fields[i];
		result[field] = data[field];
	}

	return result;
}

export function getFolderSettings(folderId) {
	const stringData = localStorage.getItem(folderId);
	if (!stringData) {
		return null;
	}

	try {
		return JSON.parse(stringData);
	} catch (e) {

	}

	return null;
}

export function updateFolderSettings(folderId, data) {
	const stringData = JSON.stringify({
		...data,
		type: 'folderSettings'
	});
	localStorage.setItem(folderId, stringData);
}


export function saveTextAsFile(fileName, type, textToSave) {
	function destroyClickedElement(event) {
		document.body.removeChild(event.target);
	}

	const textToSaveAsBlob = new Blob([textToSave], {type});
	const textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);

	const downloadLink = document.createElement("a");
	downloadLink.download = fileName;
	downloadLink.innerHTML = "Download File";
	downloadLink.href = textToSaveAsURL;
	downloadLink.onclick = destroyClickedElement;
	downloadLink.style.display = "none";
	document.body.appendChild(downloadLink);

	downloadLink.click();
}
