import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from '../reducers';
import { connectRouter, routerMiddleware } from 'connected-react-router';

export default function configureStore(history, initialState) {
	const store = createStore(connectRouter(history)(rootReducer), initialState, compose(
		applyMiddleware(
			routerMiddleware(history),
			thunk,
			logger
		),
		// add support for Redux dev tools
		// window.devToolsExtension ? window.devToolsExtension() : f => f
	));
	return store;
}
