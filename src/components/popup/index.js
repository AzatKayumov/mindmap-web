import React, {Component} from 'react';
import PropTypes from 'prop-types';

import style from './style.scss';
import ReactDOM from "react-dom";

const modalRoot = document.getElementById('modal-root');

class Popup extends Component {
	constructor(props) {
		super(props);
		this.el = document.createElement('div');
	}

	componentDidMount() {
		modalRoot.appendChild(this.el);
	}

	componentWillUnmount() {
		modalRoot.removeChild(this.el);
	}

	render() {
		return ReactDOM.createPortal(
			this.props.children,
			this.el,
		);
	}
}

Popup.propTypes = {
	isOpened: PropTypes.bool
};

Popup.defaultProps = {
	into: 'body'
};

export default Popup;
