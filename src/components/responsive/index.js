import React, {Component} from 'react';
import Responsive from "react-responsive";

export const ShowOnDesktop = props => <Responsive {...props} minWidth={1170} />;
export const ShowOnMobile = props => <Responsive {...props} maxWidth={1169} />;
