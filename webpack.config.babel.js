const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const OfflinePlugin = require('offline-plugin');
const path = require('path');
const config = require('./config');

var packageJson = require('./package.json');
var vendorDependencies = Object.keys(packageJson['dependencies']);

const ENV = process.env.NODE_ENV || 'development';
const loaderUtils = require('loader-utils');

const CSS_MAPS = ENV !== 'production';


const cssLoaderOptions = {
	modules: true,
	sourceMap: CSS_MAPS,
	importLoaders: 1,
	minimize: true,
};


if (ENV === 'development') {
	cssLoaderOptions.getLocalIdent = (context, localIdentName, localName, options) => {
		// Use the filename or folder name, based on some uses the index.js / index.module.css project style
		const fileNameOrFolder = context.resourcePath.endsWith('index.module.css') ? '[folder]' : '[name]';
		// Create a hash based on a the file location and class name. Will be unique across a project, and close to globally unique.
		const hash = loaderUtils.getHashDigest(context.resourcePath + localName, 'md5', 'base64', 5);
		// Use loaderUtils to find the file or folder name
		const className = loaderUtils.interpolateName(context, fileNameOrFolder + '_' + localName + '__' + hash, options);
		// remove the .module that appears in every classname when based on the file.
		return className.replace('.module_', '_');
	};
}

module.exports = {
	context: path.resolve(__dirname, "src"),
	entry: ["babel-polyfill", './index.js'],

	output: {
		path: path.resolve(__dirname, "build"),
		publicPath: ENV !== 'production' ? '/' : './',
		filename: 'bundle.js'
	},

	resolve: {
		extensions: ['.tsx', '.ts', '.jsx', '.js', '.json', '.scss'],
		modules: [
			path.resolve(__dirname, "src/lib"),
			path.resolve(__dirname, "node_modules"),
			'node_modules'
		],
		alias: {
			components: path.resolve(__dirname, "src/components"),    // used for tests
			containers: path.resolve(__dirname, "src/containers"),    // used for tests
			reducers: path.resolve(__dirname, "src/reducers"),    // used for tests
			actions: path.resolve(__dirname, "src/actions"),    // used for tests
			assets: path.resolve(__dirname, "src/assets"),    // used for tests
			style: path.resolve(__dirname, "src/style")
		}
	},

	module: {
		rules: [
			{
				test: /\.svg$/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							"presets": [
								"react"
							]
						},
					},
					{
						loader: 'svgr/webpack',
						options: {babel: false}
					}
				]
			},
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/
			},
			{
				test: /\.jsx?$/,
				exclude: path.resolve(__dirname, 'src'),
				enforce: 'pre',
				use: 'source-map-loader'
			},
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				use: 'babel-loader'
			},
			{
				// Transform our own .(scss|css) files with PostCSS and CSS-modules
				test: /\.(scss|css)$/,
				include: [
					path.resolve(__dirname, 'src/components'),
					path.resolve(__dirname, 'src/containers')
				],
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader',
							options: cssLoaderOptions
						},
						{
							loader: 'sass-loader',
							options: {sourceMap: CSS_MAPS}
						}
					]
				})
			},
			{
				test: /\.(scss|css)$/,
				exclude: [
					path.resolve(__dirname, 'src/components'),
					path.resolve(__dirname, 'src/containers')
				],
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader',
							options: {sourceMap: CSS_MAPS, importLoaders: 1, minimize: true, url: false}
						},
						{
							loader: `postcss-loader`,
							options: {
								sourceMap: CSS_MAPS,
								plugins: () => {
									autoprefixer({browsers: ['last 2 versions']});
								}
							}
						},
						{
							loader: 'sass-loader',
							options: {sourceMap: CSS_MAPS}
						}
					]
				})
			},
			{
				test: /\.(xml|html|txt|md)$/,
				use: 'raw-loader'
			},
			{
				test: /\.(woff2?|ttf|eot|jpe?g|png|gif)(\?.*)?$/i,
				use: ENV === 'production' ? 'file-loader' : 'url-loader'
			}
		]
	},
	plugins: ([
		new webpack.NoEmitOnErrorsPlugin(),
		new ExtractTextPlugin({
			filename: 'style.css',
			allChunks: true,
			disable: ENV !== 'production'
		}),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify(ENV)
		}),
		new HtmlWebpackPlugin({
			options: {
				appVersion: config.appVersion
			},
			template: './index.ejs',
			minify: {collapseWhitespace: true}
		}),
		new CopyWebpackPlugin([
			{from: './manifest.json', to: './'},
			{from: './favicon.ico', to: './'}
		])
	]).concat(ENV === 'production' ? [
		// new webpack.optimize.UglifyJsPlugin({
		// 	output: {
		// 		comments: false
		// 	},
		// 	compress: {
		// 		unsafe_comps: true,
		// 		properties: true,
		// 		keep_fargs: false,
		// 		pure_getters: true,
		// 		collapse_vars: true,
		// 		unsafe: true,
		// 		warnings: false,
		// 		screw_ie8: true,
		// 		sequences: true,
		// 		dead_code: true,
		// 		drop_debugger: true,
		// 		comparisons: true,
		// 		conditionals: true,
		// 		evaluate: true,
		// 		booleans: true,
		// 		loops: true,
		// 		unused: true,
		// 		hoist_funs: true,
		// 		if_return: true,
		// 		join_vars: true,
		// 		cascade: true,
		// 		drop_console: true
		// 	}
		// }),
		//
		// new OfflinePlugin({
		// 	relativePaths: false,
		// 	AppCache: false,
		// 	excludes: ['_redirects'],
		// 	ServiceWorker: {
		// 		events: true
		// 	},
		// 	cacheMaps: [
		// 		{
		// 			match: /.*/,
		// 			to: '/',
		// 			requestTypes: ['navigate']
		// 		}
		// 	],
		// 	publicPath: ENV !== 'production' ? '/' : './',
		// })
	] : []),

	stats: {colors: true},

	node: {
		global: true,
		process: false,
		Buffer: false,
		__filename: false,
		__dirname: false,
		setImmediate: false
	},

	devtool: ENV === 'production' ? 'source-map' : 'source-map',

	devServer: {
		port: process.env.PORT || 3000,
		host: process.env.HOST || '0.0.0.0',
		publicPath: '/',
		contentBase: './src',
		historyApiFallback: true,
		// open: true,
		openPage: '',
		watchOptions: {
			poll: true,
			ignored: /node_modules/
		},
		headers: {
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
			"Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
		},
		proxy: {
			// OPTIONAL: proxy configuration:
			// '/optional-prefix/**': { // path pattern to rewrite
			//   target: 'http://target-host.com',
			//   pathRewrite: path => path.replace(/^\/[^\/]+\//, '')   // strip first path segment
			// }
		}
	}
};
